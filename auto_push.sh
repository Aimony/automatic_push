#!/bin/bash

# 进入项目
cd Automatic_push/

# 生成两个随机数
random_number1=$((RANDOM % 101))
random_number2=$(( (RANDOM % 10) + 1 ))

# # 输出整个表达式的值
# output="$random_number1 % $random_number2 = $((random_number1 % random_number2))"
# # 获取当前时间并写入文件
 current_time=$(date +"%Y-%m-%d %H:%M:%S")
# # 在文件的第一行开头追加新的输出
# sed -i "1s|^|$output\\n|;1s|.*|Last updated: $current_time \"$random_number1 % $random_number2 = $((random_number1 % random_number2))\"|" README.md


# 新的内容
# new_content="Last updated: $current_time \"$random_number1 % $random_number2 = $((random_number1 % random_number2))\""


# 将新内容追加到文件的开头第一行，并保留原本的内容
# awk -v new_content="$new_content" '{print new_content "\n" $0}' README.md > temp && mv temp README.md
# awk -v new_content="$new_content" '{print (NR==1 ? new_content : "") $0}' README.md > temp && mv temp README.md


# 定义内容
content="Last updated: $current_time \"$random_number1 % $random_number2 = $((random_number1 % random_number2))\"\n"

# 将内容写入 README.md 文件的开头第一行
awk -v content="$content" 'BEGIN {print content} {print}' README.md > temp && mv temp README.md



# 添加修改到暂存区
git add .

# 提交修改
git commit -m "Automatic update: $current_time"

# 如果第一个随机数能整除第二个随机数，执行git push命令
if [ "$((random_number1 % random_number2))" -eq 0 ]; then
    # 推送到远程仓库
    git push
fi

# 返回主目录
cd ~

